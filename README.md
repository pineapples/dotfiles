![imgmyxmonadrice](https://codeberg.org/pineapples/dotfiles/raw/branch/main/xmonadscrn.png)
# My Dotfiles

- hotkeys: sxhkd used to replace usual WMs keybinds.
- WMs: dwm, openbox, compiz, xmonad (current), EXWM
- FMs: vifm, xplr, thunar, pcmanfm
- Default Shell: Zsh
- Editors: NEOVIM, EMACS
- panels: tint2, xfce4-panel, xmobar (current
- custom posix shell scripts used for general tasks. check `~/.local/bin` folder
- statusbar scripts are purely posix shellscripts
- autostarts: xprofile
- display manager: slim (not in use)
- NO LOGIN MANGER (Only TTY based login)
- select WM or DE using .xinitrc
