# This configuration works well with
# https://github.com/dccsillag/picom/tree/implement-window-animations

backend = "glx";
# corner-radius= 0.0
#################################
#          Transparancy         #
#################################

inactive-opacity = 1.0;
active-opacity = 1.0;
frame-opacity = 1.0;
inactive-opacity-override = false;
opacity-rule = [
                   "10:class_g = 'Bspwm'",
                   "10:class_i = 'presel_feedback'",
		   "82:class_g = 'dwm'",
	           "95:class_g = 'Thunar'",
	           "92:class_g = 'TelegramDesktop'",
	           "81:class_g = 'Zathura'",
	           "81:class_g = 'surf'",
	           "81:class_g = 'lynx'",
                ];

wintypes :
{
    popup_menu =
    {
        opacity = 1.0;
    };
    dropdown_menu =
    {
        opacity = 1.0
    };
    dnd =
    {
        shadow = false;
    };
    dock =
    {
        shadow = true;
    };
    tooltip =
    {
        fade = true;
        shadow = true;
        opacity = 1.0;
        focus = true;
    };
};


#################################
#          Animations           #
#################################

animations: true;

animation-stiffness = 350
animation-window-mass = 0.7
animation-dampening = 20
animation-clamping = true

animation-for-open-window = "slide-down"; #open window
animation-for-unmap-window = "slide-down"; #minimize window
animation-for-menu-window = "slide-down";
animation-for-transient-window = "slide-right"; #popup windows

animation-for-workspace-switch-in = "zoom"; #the windows in the workspace that is coming in
animation-for-workspace-switch-out = "zoom"; #the windows in the workspace that are coming out

# Fading
fading = true;
fade-delta = 5;
fade-in-step = 0.3;
fade-out-step = 0.3;


#################################
#          SHADOWS              #
#################################

shadow = true;

shadow-radius = 10;
shadow-offset-x = 3;
shadow-offset-y = -13;
shadow-opacity = 0.5;
shadow-red = 0.0;
shadow-green = 0.0;
shadow-blue = 0.0;

shadow-exclude = [

                     "name = 'stalonetray'",
                     "name = 'polybar'",
                     "name = 'plank'",
                     "name = 'Notification'",
                     "name = 'fluxbox'",
                     "class_g = 'bspwm'",
                     "class_i = 'presel_feedback'",
                     "class_g = 'fluxbox'",
                     "class_g ?= 'Notify-osd'",
                     "class_g = 'xfce4-panel'",
                     "class_i = 'Meteo'",
                  ];

shadow-ignore-shaped = false;
